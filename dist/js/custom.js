(function($){
	$('[data-toggle="tooltip"]').tooltip();

	// $(window).scroll(function(){
	//     if ($(window).scrollTop() >= 72) {
	//         $('.nav-seting').addClass('fixed-nav');
	//     }
	//     else {
	//         $('.nav-seting').removeClass('fixed-nav');
	//     }
	// });
	
	$('#table_id, #table_id2').DataTable();
	

	// mobile menu
	var openMobileMenu = function () {
	  var body = $('body');
	  if (body.hasClass('open')) {
	      closeMobileMenu();
	    } else {
	      body.addClass('open');
	    }
	  }
	  var closeMobileMenu = function () {
	    $('body').removeClass('open');
	  }

	  /* MOBILE MENU */
	  var mobileMenu = $('.js-mobile-menu');
	  $(document).on('click', '.js-mobile-button', function () {
	    openMobileMenu();
	    return false;
	  });

	  $(document).off('click.mobileMenu').on('click.mobileMenu', function (e) {
	    var t = $(e.target);
	    if (t.closest('.js-mobile-menu').length < 1 && t.closest('.js-mobile-button').length < 1) {
	  	    closeMobileMenu(mobileMenu);
	 	 }
 	  });
})(jQuery);