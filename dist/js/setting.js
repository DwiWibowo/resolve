(function($){
	$('.js-link').on('click', function(){
		$(this).parents('.nav').find('.nav-item').removeClass('active');
		$(this).parent('li').addClass('active');
	});
	$('a[href*="#"].js-link').click(function() {
		console.log($.attr(this, 'href'));
		//var that = $(this);
		$('html,body, .setting-wrapper').animate({
		  scrollTop: $( $.attr(this, 'href') ).offset().top - 20
		}, 1000);
		return false;
	 });
	
	window.onscroll = function() {myFunction()};

	var header = document.getElementById("myNav");
	var sticky = header.offsetTop;

	function myFunction() {
	  if (window.pageYOffset > sticky) {
	    header.classList.add("sticky");
	  } else {
	    header.classList.remove("sticky");
	  }
	}

})(jQuery);